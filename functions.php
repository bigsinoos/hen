<?php
/**
 * Check user input for invalid values
 *
 * @param array $lessons
 * @return array
 */
function fixInput( $lessons)
{
	$output = array();
	foreach($lessons as $id => $lesson){
		if ( empty($lesson['mass']) || $lesson['mass'] < 1 )
			$lesson['mass'] = 1;
		if ( ! empty($lesson['name']) && is_numeric($lesson['point']) && $lesson['point'] >= 0 )
			$output[] = $lesson;
	}
	return $output;
}

/**
 * Calculate averate from $_POST defined array
 *
 * @param array $data
 * @return array
 */
function calAverage( $data )
{
	if(count( $data ) == 0)
		return 0;
	$full_sum = sumWithMass($data);
	$masses = sumMasses($data);
	return ( ! $masses ? 0 : $full_sum/ $masses );
}

/**
 * sigma(m_i) : average = sigma(f_i * m_i) / sigma(m_i)
 * 
 * @param array $to_sum
 * @return numeric;
 */
function sumMasses( $to_sum)
{
	$sum = 0;
	foreach($to_sum as $item){
		$sum += $item['mass'];
	}
	return $sum;
}

/**
 * sigma(f_i * m_i) :  average = sigma(f_i * m_i) / sigma(m_i)
 *
 * @param array $to_sum
 * @return numeric
 */
function sumWithMass( $to_sum)
{
	$sum = 0;
	foreach( $to_sum as $item)
	{
		$sum += $item['point'] * $item['mass'];
	}
	return $sum;
}