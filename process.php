<?php
session_start();

require_once "functions.php";

$lessons = array(); //lessons to calculate average on

if( ! empty( $_POST['lesson'])){
	$lessons = fixInput($_POST['lesson']);
}

// Fail if invalid data was sent by user 
if( empty( $lessons )) {
	$fail_message = "شما مقدارهای صحیح و درستی وارد نکرده اید";
	$_SESSION['message'] = $fail_message;
	header("Location: index.php");
	exit();
}

// Calculate and redirect back index.php will handle anything else!
$_SESSION['avrg'] = calAverage($lessons);
header("Location: index.php");
exit();



