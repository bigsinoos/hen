<?php session_start();?>
<!DOCTYPE html>
<html lang="fa">
	<head>
		<title>محاسبه معدل</title>
		<meta charset="utf-8"/>
		<link rel="stylesheet" href="style.css"/>
		<script type="text/javascript" src="jq.js"></script>
	</head>
	<body>
		<div class="content">
			<?php if( ! empty($_SESSION['avrg'] )) { ?>
				<h1 style="color:red;text-align:center" >معدل: <?php print $_SESSION['avrg']?></h1>
			<?php } elseif ( ! empty($_SESSION['message'])) { ?>
				<div class="error" style="color:red"><?php print $_SESSION['message'];?></div>
			<?php } ?>
			<form action="process.php" method="POST" class="process">
				<table class="points">
					<thead>
						<tr>
							<th>#شناسه</th>
							<th>نام درس</th>
							<th>نمره</th>
							<th>ضریب</th>
						</tr>
					</thead>
					<tbody class="lessons">
						<tr class="lesson-row">
							<td class="lesson-id">1</td> 
							<td class="lesson-name"><input type="text" name="lesson[0][name]"></td>
							<td class="lesson-point"><input type="text" name="lesson[0][point]"></td>
							<td class="lesson-mass"><input type="text" name="lesson[0][mass]"></td>
						</tr>
					</tbody>
				</table><br>
				<button type="button" class="add-lesson">اضافه کردن درس</button>
				<button type="submit">محاسبه</button>
			</form>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				var last_lesson_id = 0;
				$('.add-lesson').click(function(){
					var new_lesson;
					last_lesson_id++;
					var id = '<td>' + (last_lesson_id + 1) + '</td>';
					var name = '<td class="lesson-id"><input type="text" name="lesson[' + last_lesson_id +'][name]"></td>';
					var point = '<td class="lesson-point"><input type="text" name="lesson[' + last_lesson_id +'][point]"></td>';
					var mass = '<td class="lesson-mass"><input type="text" name="lesson[' + last_lesson_id +'][mass]"></td>';
					var tr = '<tr>' + id + name + point + mass + '</tr>';
					$('.points').append(tr);
				});
			});
		</script>
	</body>
</html>
<?php session_destroy();?>